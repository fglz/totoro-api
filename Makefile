lint:
	pre-commit run -a -v

test:
	pytest -sx totoro-api

runserver:
	python totoro-api/manage.py runserver 0.0.0.0:8000

docker-test:
	docker run --rm --env-file .env -v `pwd`:/home/app/ --entrypoint pytest totoro-api -sx totoro-api

docker-build:
	docker build -t "totoro-api" .

no_cache_build:
	docker build --no-cache -t "totoro-api" .

docker-run:
	docker run -it -p 8000:8000 "totoro-api" python totoro-api/manage.py runserver 0.0.0.0:8000
