import time

import pytest
import responses
from rest_framework import status
from rest_framework.reverse import reverse

pytestmark = pytest.mark.django_db


def test_get_movie_list(test_client):
    url = reverse("movies:movies-list")
    response = test_client.get(url)
    assert response.status_code == status.HTTP_200_OK


@responses.activate
def test_caching(test_client):
    responses.add(
        responses.GET, "https://ghibliapi.herokuapp.com/films", json={}, status=200
    )
    responses.add(
        responses.GET, "https://ghibliapi.herokuapp.com/people", json={}, status=200
    )

    url = reverse("movies:movies-list")
    result_a = test_client.get(url)
    result_b = test_client.get(url)
    assert result_a.json()["last_updated_at"] == result_b.json()["last_updated_at"]


@responses.activate
def test_caching_timeout(test_client):
    responses.add(
        responses.GET, "https://ghibliapi.herokuapp.com/films", json={}, status=200
    )
    responses.add(
        responses.GET, "https://ghibliapi.herokuapp.com/people", json={}, status=200
    )

    url = reverse("movies:movies-list")
    result_a = test_client.get(url)
    time.sleep(1)
    result_b = test_client.get(url)
    assert result_a.json()["last_updated_at"] != result_b.json()["last_updated_at"]
