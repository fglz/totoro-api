from unittest import mock

import pytest

from ..client import ClientException
from ..models import Movie


def test_get_movies(ghibli_client):
    with mock.patch(
        "apps.movies.client.GhibliClient._request"
    ) as mock_request, mock.patch(
        "apps.movies.client.GhibliClient._format_movies"
    ) as mock_format_movies, mock.patch(
        "apps.movies.client.GhibliClient._add_characters_to_movies"
    ) as mock_add_characters_to_movies:
        mock_add_characters_to_movies.return_value = "movie_list"
        response = ghibli_client.get_movies()
    assert response["movies"] == "movie_list"
    assert mock_request.call_count == 2
    assert mock_format_movies.call_count == 1
    assert mock_add_characters_to_movies.call_count == 1


def test_add_characters_to_movie(ghibli_client, movies_payload, characters_payload):
    movie = movies_payload[0]
    movies_mapping = {movie["id"]: Movie(**movie)}

    response = ghibli_client._add_characters_to_movies(
        characters_response=characters_payload, movies_mapping=movies_mapping
    )
    assert response[0]["id"] == movie["id"]
    character = response[0]["characters"][0]
    assert character["name"] == characters_payload[0]["name"]
    assert character["id"] == characters_payload[0]["id"]


def test_add_characters_to_movie_no_characters(ghibli_client, movies_payload):
    movie = movies_payload[0]
    movies_mapping = {movie["id"]: Movie(**movie)}

    response = ghibli_client._add_characters_to_movies(
        characters_response=[], movies_mapping=movies_mapping
    )
    assert response[0]["characters"] == []
    assert response[0]["id"] == movie["id"]


def test_format_movies(ghibli_client, movies_payload):
    response = ghibli_client._format_movies(movies_payload)

    assert movies_payload[0]["id"] in response.keys()
    movie = response[movies_payload[0]["id"]]
    assert type(movie) == Movie
    assert movie.characters == []
    assert movie.id == movies_payload[0]["id"]


def test_format_movies_no_movies(ghibli_client):
    response = ghibli_client._format_movies([])
    assert response == {}


def test_request(ghibli_client, mock_request_response, movies_payload):
    with mock.patch("apps.movies.client.requests.get") as mock_request:
        mock_request.return_value = mock_request_response
        url = "test_url"
        response = ghibli_client._request(url)
    assert response == movies_payload


def test_request_client_exception(ghibli_client, mock_request_response):
    mock_request_response.status_code = 400

    with mock.patch("apps.movies.client.requests.get") as mock_request:
        mock_request.return_value = mock_request_response
        url = "test_url"
        with pytest.raises(ClientException):
            ghibli_client._request(url)
