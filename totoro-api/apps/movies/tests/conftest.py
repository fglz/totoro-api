import pytest
from rest_framework.test import APIClient

from ..client import GhibliClient


@pytest.fixture
def test_client():
    test_client = APIClient()
    return test_client


@pytest.fixture
def mock_request_response(movies_payload):
    class Request:
        status_code = 200
        json_payload = movies_payload

        def json(self):
            return self.json_payload

    return Request()


@pytest.fixture
def ghibli_client():
    return GhibliClient()


@pytest.fixture
def characters_payload():
    return [
        {
            "id": "ba924631-068e-4436-b6de-f3283fa848f0",
            "name": "Ashitaka",
            "gender": "male",
            "age": "late teens",
            "eye_color": "brown",
            "hair_color": "brown",
            "films": [
                "https://ghibliapi.herokuapp.com/films/0440483e-ca0e-4120-8c50-4c8cd9b965d6"
            ],
            "species": "https://ghibliapi.herokuapp.com/species/af3910a6-429f-4c74-9ad5-dfe1c4aa04f2",
            "url": "https://ghibliapi.herokuapp.com/people/ba924631-068e-4436-b6de-f3283fa848f0",
        }
    ]


@pytest.fixture
def movies_payload():
    return [
        {
            "id": "0440483e-ca0e-4120-8c50-4c8cd9b965d6",
            "title": "Princess Mononoke",
            "description": "Ashitaka, a prince of the disappearing Ainu tribe, is cursed by a demonized boar god[...]",
            "director": "Hayao Miyazaki",
            "producer": "Toshio Suzuki",
            "release_date": "1997",
            "rt_score": "92",
        }
    ]
