from dataclasses import dataclass
from dataclasses import fields


@dataclass
class BaseDataClass:
    def __init__(self, **kwargs):
        names = set([f.name for f in fields(self)])
        for k, v in kwargs.items():
            if k in names:
                setattr(self, k, v)


@dataclass
class Character(BaseDataClass):
    id: str
    name: str
    gender: str
    eye_color: str
    hair_color: str
    age: str = "Unknown"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


@dataclass
class Movie(BaseDataClass):
    id: str
    title: str
    description: str
    director: str
    producer: str
    release_date: str
    rt_score: str
    characters: list

    def __init__(self, **kwargs):
        # Had to do it, sorry
        kwargs["characters"] = []
        super().__init__(**kwargs)
