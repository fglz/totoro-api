from dataclasses import asdict
from datetime import datetime

import requests
from rest_framework import status

from .models import Character
from .models import Movie


class ClientException(Exception):
    message = "Ghibli API not available"


class GhibliClient:
    _movies_url = "https://ghibliapi.herokuapp.com/films"
    _characters_url = "https://ghibliapi.herokuapp.com/people"

    def get_movies(self):
        movies_response = self._request(self._movies_url)
        movies_mapping = self._format_movies(movies_response)
        characters_response = self._request(self._characters_url)
        movies = self._add_characters_to_movies(characters_response, movies_mapping)
        return {"last_updated_at": datetime.now(), "movies": movies}

    def _add_characters_to_movies(self, characters_response, movies_mapping):
        for character_json in characters_response:
            character = Character(**character_json)
            for movie_url in character_json["films"]:
                movie_id = movie_url.split("/")[-1]
                movies_mapping[movie_id].characters.append(character)
        movies = list(movies_mapping.values())
        return [asdict(movie) for movie in movies]

    def _format_movies(self, movies_response):
        return {movie["id"]: Movie(**movie) for movie in movies_response}

    def _request(self, url):
        response = requests.get(url)
        if response.status_code != status.HTTP_200_OK:
            raise ClientException
        return response.json()
