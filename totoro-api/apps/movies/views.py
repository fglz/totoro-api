from django.conf import settings
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response

from .client import ClientException
from .client import GhibliClient


class MovieViewSet(viewsets.ViewSet):
    client = GhibliClient

    @method_decorator(cache_page(settings.CACHING_TIMEOUT))
    def list(self, request, *args, **kwargs):
        client = GhibliClient()
        try:
            movies = client.get_movies()
        except ClientException:
            return Response(status=status.HTTP_503_SERVICE_UNAVAILABLE)
        return Response(movies, status=status.HTTP_200_OK)
