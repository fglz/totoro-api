from rest_framework import routers

from .views import MovieViewSet

app_name = "movies"

routes = routers.DefaultRouter()
routes.register(r"movies", MovieViewSet, basename="movies")

urlpatterns = routes.urls
