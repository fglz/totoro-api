# Totoro API

*Totoro API* is an integrates with *Studio Ghibli API* (https://ghibliapi.herokuapp.com/) to display movie and characters in a formatted way, caching the requests to avoid response delay. Caching can be customized by environment variables, but defaults to 60 seconds.


Installing
----------

The project can either be run locally or with Docker in build container

### Docker

Running the project with docker is very straightforward:

- Make sure you have docker installed (https://docs.docker.com/get-docker/)
- Build the docker container by running:

.. code:: bash

    make docker-build

- Run the container by running:

.. code:: bash

    make docker-run

- Access the page http://0.0.0.0:8000/movies/ to see the results!

### Locally

Running the project locally is also very simple:

- Install requirements using pip:

.. code:: bash

    pip install -r requirements.txt

- Or poetry (https://python-poetry.org/)

.. code:: bash

    poetry install
    poetry shell #Starts virtual environment

- Run the server:

.. code:: bash

    make runserver

- Access the page http://0.0.0.0:8000/movies/ to see the results!


Tests
-------

#### Docker
.. code:: bash

    make docker-test
#### Locally
.. code:: bash

    make test
